import React, { Component } from 'react';
import 'weather-icons/css/weather-icons.css';
class Daycard extends Component {
    // console.log()
    // const imgURL = `owf owf-${reading.weather[0].id} owf-5x`
constructor(){


super();
this.state = {
    icons: '',
    temp: undefined,
    feels_like: undefined,
    temp_min: undefined,
    temp_max: undefined,
    pressure: undefined,
    humidity: undefined,
    dayname:undefined,
    description: '',
    // icons: ''
  }
 
  this.getWeathericons()

    this.weathericons = {
        Thunderstorm: 'wi-thunderstorm',
        Drizzle: 'wi-sleet',
        Rain: 'wi-storm-showers',
        Show: 'wi-snow',
        Atmosphere: 'wi-fog',
        Clear: 'wi-day-sunny',
        Clouds: 'wi-day-fog',
        // icons:''
  
      }
    //   getWeatherData1
    }
    getWeathericons(icons, rangeId) {
        // icons:this.weathericons.Thunderstorm
    
        switch (true) {
          case rangeId >= 800:
            this.setState({
              icons: this.weathericons.Thunderstorm
            })
            break;
          case rangeId >= 300 && rangeId <= 321:
            this.setState({
              icons: this.weathericons.Drizzle
            })
            break;
          case rangeId >= 500 && rangeId <= 531:
            this.setState({
              icons: this.weathericons.Rain
            })
            break;
          case rangeId >= 600 && rangeId <= 622:
            this.setState({
              icons: this.weathericons.Show
            })
            break;
          case rangeId >= 700 && rangeId <= 781:
            this.setState({
              icons: this.weathericons.Atmosphere
            })
            break;
          case rangeId === 800:
            this.setState({
              icons: this.weathericons.Clear
            })
            break;
          case rangeId >= 801 && rangeId <= 804:
            this.setState({
              icons: this.weathericons.Clouds
            })
            break;
          default:
            this.setState({
              icons: 'wi-thunderstorm'
            })
    
        }
      }


 getWeatherData1=()=>{
     this.getWeathericons(this.weathericons,this.props.reading.weather[0].id)
    console.log(this.props)

    
        // let t1 = Math.floor(temp - 273.15)
        // return t1
    
 }

 calculatetempindegree=() =>{

    this.setState({
     temp:Math.round(this.props.reading.main.temp-273.15),
     temp_max:Math.round(this.props.reading.main.temp_max-273.15),
 
   

    })
  

}

Dayanme=()=>{
var weekdays=['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
var date=new Date(this.props.reading.dt*1000)
var dayName=weekdays[date.getDay()]
; // It will give day index, and based on index we can get day name from the array. 
console.log(dayName)
console.log(date)
this.setState({
    dayname:dayName
})
 

}




   


 componentDidMount(){
    this.getWeatherData1()
    this.calculatetempindegree()
    this.Dayanme()

  

 }

   
    render() { 


       
// console.log(this.props)
        return (

            <div className="col-sm-2">
              <div className="daycard">
                 <p>{this.state.dayname}</p>
              
                 <i className={`wi ${this.state.icons} display-1`}></i>
                 
                {/* <h2>{Math.round(this.props.reading.main.temp)} °F</h2>  */}
                <div className="card-body">
                <h2>{this.state.temp}&deg;</h2> 
                <div>
      <span >{this.state.temp_min}&deg;</span>
      <span >{this.state.temp_max}&deg;</span>
    </div>
                  <p className="card-text">{this.props.reading.weather[0].description}</p>
                  <h4>{console.log(this.props.reading)}</h4>
                  {/* <i className="owf owf-803 owf-5x"></i> */}
                </div>
              </div>
            </div>
          )
     
    }
}
 
export default Daycard;
