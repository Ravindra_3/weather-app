import React, { Component } from "react";




function minmaxtemp(min, max) {
    return (
      <h3>
        <span className="px-3">{min}&deg;</span>
        <span className="px-4">{max}&deg;</span>
      </h3>
    );
  }

  
const Weather = (props) => {
  return (
    <div className="container">
      <div className="cards">

      <h1>Weather App</h1>
        <h1>
          {props.city}, {props.country}
        </h1>
      
  <h3>{props.dayname}</h3>
        <h5 className="py-4">
          <i className={`wi ${props.icons} display-1`}></i>
        </h5>
        <h1 className="py-2">{props.temp_celsius}&deg;</h1>
        {minmaxtemp(props.temp_min, props.temp_max)}
        <h4>{props.description}</h4>
      </div>
    </div>
  );
};


export default Weather;
