import React, {
  Component
} from 'react';
// import logo from './logo.svg';
import './App.css';
import 'weather-icons/css/weather-icons.css';
import Weather from './Componenet/weather.component'
import Weekcontainer from './Componenet/week'
import Form  from './Componenet/form.jsx'


const apikey = '1cd7cacae4f033c9efb199a32b1f3a03'
// api.openweathermap.org/data/2.5/weather?q=London,uk

class App extends Component {
  constructor() {
    super();
    this.state = {
      city: '',
      country: undefined,
      main: undefined,
      temp: undefined,
      feels_like: undefined,
      temp_min: undefined,
      temp_max: undefined,
      pressure: undefined,
      humidity: undefined,
      description: '',
      icons: ''
    }
    
    this.getWeatherData()
    this.weathericons = {
      Thunderstorm: 'wi-thunderstorm',
      Drizzle: 'wi-sleet',
      Rain: 'wi-storm-showers',
      Show: 'wi-snow',
      Atmosphere: 'wi-fog',
      Clear: 'wi-day-sunny',
      Clouds: 'wi-day-fog',
      // icons:''

    }
  }

  calculatetempindegree(temp) {
    let t1 = Math.floor(temp - 273.15)
    return t1
  }
  Dayanme=(response)=>{
    var weekdays=['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    var date=new Date(response.dt*1000)
    var dayName=weekdays[date.getDay()]
    return dayName
  
 
  }
  getWeathericons(icons, rangeId) {
    // icons:this.weathericons.Thunderstorm

    switch (true) {
      case rangeId >= 800:
        this.setState({
          icons: this.weathericons.Thunderstorm
        })
        break;
      case rangeId >= 300 && rangeId <= 321:
        this.setState({
          icons: this.weathericons.Drizzle
        })
        break;
      case rangeId >= 500 && rangeId <= 531:
        this.setState({
          icons: this.weathericons.Rain
        })
        break;
      case rangeId >= 600 && rangeId <= 622:
        this.setState({
          icons: this.weathericons.Show
        })
        break;
      case rangeId >= 700 && rangeId <= 781:
        this.setState({
          icons: this.weathericons.Atmosphere
        })
        break;
      case rangeId === 800:
        this.setState({
          icons: this.weathericons.Clear
        })
        break;
      case rangeId >= 801 && rangeId <= 804:
        this.setState({
          icons: this.weathericons.Clouds
        })
        break;
      default:
        this.setState({
          icons: this.weathericons.Clouds
        })

    }
  }
  getWeatherData = async (e) => {
       
   const city=e||'Bangalore'
    const data = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apikey}`)
    const response = await data.json();

    console.log(e)
    this.setState({
      city: response.name,
      country: response.sys.country,
      temp: this.calculatetempindegree(response.main.temp),
      temp_max: this.calculatetempindegree(response.main.temp_max),
      temp_min: this.calculatetempindegree(response.main.temp_min),
      description: response.weather[0].description,
      dayname:this.Dayanme(response)

      // icons:this.weathericons.Thunderstorm

    })
    this.getWeathericons(this.weathericons, response.weather[0].id)

    // console.log(respo);

  }

  buttonPressed=(e)=>{
   
   
    // this.setState({
    //   city:e
    // })
    this.getWeatherData(e)
  
  }

  componentDidMount=()=>{
    this.buttonPressed()
  }


  render() {
    return ( < div className = "App" >
     
       <Form buttonpressd={this.buttonPressed} />
      <
      Weather city = {
        this.state.city
      }
      country = {
        this.state.country
      }
      temp_celsius = {
        this.state.temp
      }
      temp_min = {
        this.state.temp_min
      }
      temp_max = {
        this.state.temp_max
      }
      description = {
        this.state.description
      }
      icons = {
        this.state.icons
      }
    dayname={
      this.state.dayname
    }
      /> 
    <div >
      <Weekcontainer
      city={this.state.city}/>
      </div>
      </div> );
    }
  }

  export default App;
  // 12345